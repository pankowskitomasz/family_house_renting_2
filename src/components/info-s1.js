import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";
import Button from "../../node_modules/react-bootstrap/Button";

class InfoS1 extends Component{
    render(){
        return(    
            <Container fluid className="info-s1 d-flex minh-50vh align-items-center py-5">
            <Row className="mx-0 w-100 mt-5">
                <Col xs={{span:12,order:1}} md={{span:7,order:2}} className="minh-25vh"></Col>
                <Col xs={{span:12,order:2}} md={{span:5,order:1}} className="minh-50vh d-flex align-items-center">
                    <div className="w-100 text-center text-md-start">
                        <h5 className="text-white">
                            Should You 
                        </h5>
                        <h2 className="dispaly-6 fw-bold text-white-50 text-shadow">
                            Invest In San Diego
                        </h2>
                        <p className="text-white">
                            Choosing between investing in San Diego single-family homes or multi-family 
                            rentals requires knowing your investment goals. What’s more important: 
                            appreciation or cash flow? Let’s explore these and other goals.
                        </p>
                    </div>
                </Col>
            </Row>
        </Container>
        );
    }
}

export default InfoS1;