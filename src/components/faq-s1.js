import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";
import Button from "../../node_modules/react-bootstrap/Button";

class FaqS1 extends Component{
    render(){
        return(    
            <Container fluid className="faq-s1 d-flex minh-50vh align-items-center py-5 border-top border-secondary">
            <Row className="mx-0 w-100 mt-5">
                <Col xs={{span:12,order:1}} md={{span:8,order:2}} className="minh-25vh"></Col>
                <Col xs={{span:12,order:2}} md={{span:4,order:1}} className="minh-50vh d-flex align-items-center">
                    <div className="w-100 text-center text-md-start">
                        <h2 className="dispaly-6 fw-bold text-white-50 text-shadow">
                            Need Help?
                        </h2>
                        <p className="text-white">
                            Our company offers experienced Realtors in San Diego County to help you find 
                            the best single-family home rentals. For example, read our informative blog 
                            post: House Hunting San Diego Questions 2021. Contact now to see what homes 
                            meet your investment goals.
                        </p>
                    </div>
                </Col>
            </Row>
        </Container>
        );
    }
}

export default FaqS1;