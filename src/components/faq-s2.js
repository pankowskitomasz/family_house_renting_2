import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";
import Table from "../../node_modules/react-bootstrap/Table";

class FaqS2 extends Component{
    render(){
        return(    
            <Container fluid className="d-flex minh-50vh align-items-center py-5 bg-dark">
                <Row className="mx-0 w-100 px-3 text-shadow">
                    <Col xs={12} className="py-3">
                        <h2 className="display-6 text-center text-md-start text-white text-uppercase">
                            Simpler Property Management
                        </h2>
                    </Col>
                    <Col xs={12} md={3} className="mx-auto text-center text-md-start">
                        <p className="initialism text-white">
                            A single-family San Diego home only produces one income. Contrary, 
                            multiple-family units produce several incomes. One vacancy doesn’t 
                            hurt your income as does renting a single-family home. Living in one 
                            of the units while renting the others lowers your interest rates in 
                            buildings with 2 to 4 units.
                        </p>
                    </Col>
                    <Col md={1} className="d-none d-md-block mx-auto">
                        <div class=" border-start mx-auto h-100 w-25"></div>
                    </Col>
                    <Col xs={12} md={3} className="text-center text-md-start">
                        <p className="initialism text-white">
                            You only need to manage one building with multiple units. This results 
                            in paying for repairing one roof, one pest control bill, and maintaining 
                            one yard. This makes managing your investment simpler. It takes less 
                            time to manage one building than several. One visit to meet multiple 
                            tenants.
                        </p>
                    </Col>
                    <Col md={1} className="d-none d-md-block mx-auto">
                        <div class=" border-start mx-auto h-100 w-25"></div>
                    </Col>
                    <Col xs={12} md={3} className="mx-auto text-center text-md-start">
                        <p className="initialism text-white">
                            A multi-family unit rents for much less than a home. This means more 
                            potential tenants who can afford to rent your units. This minimizes 
                            vacancy rates. Recent studies show those baby boomers as they age move 
                            into complexes rather than homes. Also, most Millenials tend to rent.
                        </p>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default FaqS2;