import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";
import Button from "../../node_modules/react-bootstrap/Button";

class GalleryS1 extends Component{
    render(){
        return(    
            <Container fluid className="gallery-s1 d-flex minh-50vh align-items-center py-5 border-top border-secondary">
            <Row className="mx-0 w-100 mt-5">
                <Col xs={{span:12,order:1}} md={{span:7,order:2}} className="minh-25vh"></Col>
                <Col xs={{span:12,order:2}} md={{span:5,order:1}} className="minh-50vh d-flex align-items-center">
                    <div className="w-100 text-center text-md-start text-shadow">
                        <h2 className="dispaly-6 fw-bold text-white">
                            Affordability
                        </h2>
                        <p className="text-white">
                            Single-family home costs less than a multi-family building. As a result, 
                            the down payment is lower with fewer maintenance costs. Utility costs are 
                            cheaper and easier to control. Insurance is cheaper for a home than a 
                            building with multiple units. Likewise, unless you have at least 20% equity, 
                            you must carry mortgage insurance for a multiplex.
                        </p>
                    </div>
                </Col>
            </Row>
        </Container>
        );
    }
}

export default GalleryS1;