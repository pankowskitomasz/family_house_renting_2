import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";

class GalleryS2 extends Component{
    render(){
        return(    
            <Container fluid className="gallery-s2 d-flex bg-secondary minh-50vh align-items-center py-5">
                <Row className="mx-0 w-100">
                    <Col xs={12} md={8} className="minh-25vh d-none d-md-block"></Col>
                    <Col xs={12} md={4} className="minh-50vh d-flex align-items-center">
                        <div className="w-100 text-center text-md-start pt-5">
                            <h2 className="dispaly-6 fw-bold text-dark">
                                Less Maintenance       
                            </h2>
                            <p className="text-secondary">
                                Common areas in buildings require more maintenance. Like the entry, 
                                elevators, stairs, hallways, and outdoor amenities. On the other hand, 
                                a home becomes the responsibility of the tenant to clean and maintain 
                                the yard. Quality tenants take pride in their home resulting in less 
                                maintenance, repairs, and wear and tear.      
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default GalleryS2;