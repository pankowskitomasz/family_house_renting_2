import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";

class FaqS3 extends Component{
    render(){
        return(    
            <Container fluid className="faq-s3 d-flex bg-light minh-50vh align-items-center py-5">
                <Row className="mx-0 w-100">
                    <Col xs={12} md={8} className="minh-25vh d-none d-md-block"></Col>
                    <Col xs={12} md={4} className="minh-50vh d-flex align-items-center">
                        <div className="w-100 text-center text-md-start pt-5 text-shadow">
                            <h2 className="dispaly-6 fw-bold text-white-50">
                                Greater Control Over Value     
                            </h2>
                            <p className="text-white">
                                The value of multiple family buildings is determined by income. Contrarily, 
                                a single-family home rental value is determined by comparable rentals and 
                                sales. Thus, you control value by maintaining and upgrading a building and 
                                raising rents according to the increased value of your building. 
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default FaqS3;