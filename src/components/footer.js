import React,{Component} from "react";
import {Link} from "react-router-dom";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";

class Footer extends Component{
    render(){
        return(
            <footer>
                <Container fluid className="d-flex text-dark align-items-center bg-dark text-white pt-3 opacity-9 border-top">
                    <Row className="mx-0 w-100 small opacity-9">
                        <Col xs={12} md={5} lg={4} className="text-center text-md-start">
                            <img src="img/navbar_logo.png" className="img-fluid mb-3" alt="logo"/>
                            <p className="initialism fw-normal">
                                Contact us with assistance for deciding on your rental investment goals, 
                                finding good rental properties, and taking all the stress of land-lording 
                                off your hands to enjoy your investment profits.
                            </p>
                        </Col>
                        <Col xs={12} md={7} lg={8} className="text-center text-md-end">                    
                            <ul className="list-inline">
                                <li className="list-inline-item">
                                    <Link to="#">
                                        <span className="fa fa-facebook text-white"></span>
                                    </Link>
                                </li>
                                <li className="list-inline-item">
                                    <Link to="#">
                                        <span className="fa fa-instagram text-white"></span>
                                    </Link>
                                </li>
                                <li className="list-inline-item">
                                    <Link to="#">
                                        <span className="fa fa-twitter text-white"></span>
                                    </Link>
                                </li>
                            </ul>       
                        </Col>
                        <Col xs={12} className="text-center border-top">
                            <p className="mb-1">
                                Copyright &copy; 2021-2022 Tomasz Pankowski. 
                                <Link href="privacy.html" className="fw-bold text-white text-decoration-none">
                                    Privacy policy
                                </Link>
                            </p>
                        </Col>
                    </Row>
                </Container>
            </footer>
        );
    }
}

export default Footer;  