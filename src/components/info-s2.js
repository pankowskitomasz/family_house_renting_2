import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";

class InfoS2 extends Component{
    render(){
        return(    
            <Container fluid className="info-s2 d-flex bg-secondary minh-50vh align-items-center py-5 border-top border-secondary">
                <Row className="mx-0 w-100">
                    <Col xs={12} md={8} className="minh-25vh d-none d-md-block"></Col>
                    <Col xs={12} md={4} className="minh-50vh d-flex align-items-center">
                        <div className="w-100 text-center text-md-start pt-5 text-shadow">
                            <h5 className="text-white">
                                The Top Benefit of 
                            </h5>
                            <h2 className="dispaly-6 fw-bold text-white-50 text-shadow">
                                Single-Family Home Rentals        
                            </h2>
                            <p className="text-white">
                                If you chose “appreciation” as your investment goal then single-family homes 
                                offer you just that. It requires holding onto the property for at least 10 
                                years. After that time, if you maintained the home in good condition with 
                                necessary upgrades the home’s value on its own should appreciate. On top of 
                                that, if you kept high occupancy levels the value of your investment should 
                                increase as well.
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default InfoS2;