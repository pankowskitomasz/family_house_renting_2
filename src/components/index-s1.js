import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";

class IndexS1 extends Component{
    render(){
        return( 
            <Container fluid className="d-flex minh-50vh align-items-center py-5">
                <Row className="mx-0 w-100 mt-5">
                    <Col xs={12} md={7} className="minh-25vh d-none d-md-block"></Col>
                    <Col xs={12} md={5} className="minh-50vh d-flex align-items-center">
                        <div className="w-100 text-center text-md-start pt-5 text-shadow">
                            <h5 className="text-white">
                                Find the best
                            </h5>
                            <h2 className="dispaly-6 fw-bold text-white-50 text-shadow">
                                House for you         
                            </h2>
                            <p className="text-white lead">
                                San Diego offers great rental investment opportunities. The question 
                                you should ask is, “Should you invest in San Diego single-family or 
                                multifamily rentals?”
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default IndexS1;
