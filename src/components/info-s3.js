import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Row from "../../node_modules/react-bootstrap/Row";
import Col from "../../node_modules/react-bootstrap/Col";

class InfoS3 extends Component{
    render(){
        return( 
            <Container fluid className="info-s3 d-flex minh-50vh align-items-center py-5 border-top border-secondary">
                <Row className="mx-0 w-100 px-3 text-shadow">
                    <Col xs={12} className="py-3">
                        <h2 className="display-6 text-center text-md-start text-white text-uppercase">
                            Vacancy Risk
                        </h2>
                    </Col>
                    <Col xs={12} md={3} className="mx-auto text-center text-md-start">
                        <p className="initialism text-white">
                            Do you like to take business risks? Currently, due to COVID-19 and eviction 
                            moratoriums, not many vacancies occur in San Diego. Yet, as you can see on 
                            our WeLease website, vacancies exist.
                        </p>
                    </Col>
                    <Col md={1} className="d-none d-md-block mx-auto">
                        <div class=" border-start mx-auto h-100 w-25"></div>
                    </Col>
                    <Col xs={12} md={3} className="text-center text-md-start">
                        <p className="initialism text-white">
                            Hopefully, by the time you buy a home or a multifamily building the pandemic 
                            ends and life in San Diego County returns to normal. Then, you face the risk 
                            of tenants moving out, vacancies lingering, and your only income comes from 
                            the remaining tenants.
                        </p>
                    </Col>
                    <Col md={1} className="d-none d-md-block mx-auto">
                        <div class=" border-start mx-auto h-100 w-25"></div>
                    </Col>
                    <Col xs={12} md={3} className="mx-auto text-center text-md-start">
                        <p className="initialism text-white">
                            A disadvantage of investing in a single-family home is if the tenant moves 
                            out your only income dries until you find another tenant. However, if you 
                            invest in a duplex or a fourplex or multifamily building you won’t lose all 
                            your income if one tenant moves out.
                        </p>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default InfoS3;